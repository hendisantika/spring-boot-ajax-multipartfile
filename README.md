# Spring Boot Ajax Upload Multipart File


I. Technologies

    * Java 8

    * Maven 3.6.1

    * IntelliJ IDEA Ultimate 2018.2

    * Spring Boot: 1.5.17.RELEASE

    * JQuery

    * Bootstrap

II. Practice

    Step to do:

    * Create SpringBoot project

    * Create Storage Service

    * Implement upload controller

    * Implement Post/Get Jquery Ajax

    * Create upload page

    * Init Storage for File System

    * Run and check results


III. Implement upload controller

    Create a RestUploadController with 3 RequestMapping:

    * @PostMapping("/api/uploadfile") is used to upload files.

    * @GetMapping("/getallfiles") is used to get all uploaded files

    * @GetMapping("/files/{filename:.+}") is used to download files.

### Screenshot

Upload File

![Home Page](img/home.png "Home page")

Get Uploaded File

![Get Uploaded File](img/get.png "Get Uploaded file")
