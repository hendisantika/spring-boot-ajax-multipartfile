package com.hendisantika.ajax;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by hendisantika on 7/18/17.
 */
@Controller
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "upload";
    }
}
